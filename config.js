module.exports = {
  // directory needed by the script
  DEBUG: true,
  DEBUG_LEVEL: 2,
  ensuredDirs: [
    'extracted',
    'src',
    'dest'
  ],
  paths: {
    testP: './testOutput/'
  }
}
