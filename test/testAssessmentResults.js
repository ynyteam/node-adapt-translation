var funcName = 'assessmentResults'
var fs = require('fs-extra')
var path = require('path')
var test = require('tape')

var helpers = require('../lib/helpers')
var checkVal = helpers.checkVal

var transform = require('../lib/cases/assessmentResults')
var filePath = './testFiles/samples/'

var testData = fs.readJsonSync(
  path.join(__dirname, filePath, funcName + '.json'), 'utf8')

var translationArray = []

var conf = {
  o: testData,
  t: translationArray
}

var testObj = transform(conf)
console.log(testObj)

function runTests () {
  test('is it a valid lodash template test', function (t) {
    t.plan(1)
    t.equal(checkVal(testObj.o.title), true, 'title is a valid template string')
  })
  test.onFinish(finished)
}
runTests()

function finished () {
  console.log(testObj._items)
}

var config = require('../config')
var tPath = config.paths.testP
console.log(tPath)

fs.ensureDir(__dirname + tPath, function (err) {
  if (err) console.log(err)
  fs.outputJsonSync(path.join(__dirname, tPath, funcName + '.json'), testObj)
})

module.exports = runTests
