var helpers = require('../lib/helpers')
var checkVal = helpers.checkVal

var commonTests = {
  isValidTemplate: function (val, t) {
    if (val !== '') {
      var msg = val + ' is a valid template string'
      t.equal(checkVal(val), true, msg)
    } else t.pass('empty string')
  }
}

module.exports = commonTests
