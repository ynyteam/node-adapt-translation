var funcName = 'textinput'
var fs = require('fs-extra')
var path = require('path')
var test = require('tape')
var Validator = require('jsonschema').Validator
var valFunc = new Validator()
var schema = fs.readFileSync(path.join(__dirname, '../schema/text.schema'))

var helpers = require('../lib/helpers')
var checkVal = helpers.checkVal

var transform = require('../lib/cases/text')
var filePath = './testFiles/samples/'

var testData = fs.readJsonSync(
  path.join(__dirname, filePath, funcName + '.json'), 'utf8')

var conf = {
  o: testData,
  t: []
}

var testObj = transform(conf)

// json schema validation
var schResults = valFunc.validate(conf.o, schema)
// console.log(schResults.errors)

function runTests () {
  test('checking component type ' + funcName, function (t) {
    t.plan(2)
    t.equal(testObj.o._type, 'component')
    t.equal(testObj.o._component, funcName)
  })

  test('schema output test', function (t) {
    t.plan(2)
    t.equal(Array.isArray(schResults.errors), true)
    t.equal(schResults.errors.length, 0)
  })
  test('is it a valid lodash template test', function (t) {
    t.plan(3)
    t.equal(checkVal(testObj.o.title), true, 'title is a not a valid template string')
    t.equal(checkVal(testObj.o.displayTitle), true)
    t.equal(checkVal(testObj.o.body), true)
  })
}
runTests()

var config = require('../config')
var tPath = config.paths.testP

fs.ensureDir(__dirname + tPath, function (err) {
  if (err) console.log(err)
  fs.outputJsonSync(path.join(__dirname, tPath, funcName + '.json'), testObj)
})

module.exports = runTests
