const fs = require('fs-extra')
const _ = require('lodash')
const path = require('path')
const nb = require('node-beautify')
const fileP = './extracted/trad/trad.json'
const outDest = path.join(__dirname, '/dest/')
const trad = fs.readJSONSync(fileP, 'utf8')
const i = require('./lib/majordome')
const clean = i.bal

const tradObj = {}

function makeTrO (arr, obj) {
  clean([outDest])
  fs.ensureDirSync(path.join(outDest, 'course/en/'))
  _.forEach(arr, function (o, k) {
    _.forEach(o, function (v, k) {
      console.log('v', v, 'k', k)
      obj[k] = v
    })
  })

  replaceObj('course/config.json', obj)
  replaceObj('course/en/course.json', obj)
  replaceObj('course/en/components.json', obj)
  replaceObj('course/en/contentObjects.json', obj)
  replaceObj('course/en/articles.json', obj)
  return obj
}


function gfObj (file) {
  return fs.readFileSync(path.join(__dirname, '/extracted/', file), 'UTF8')
}

function replaceObj (file, data) {
  var destPath = path.join(outDest, file)
  var template = _.template(gfObj(file))
  var rendered = template(data)

  fs.writeFile(destPath, nb.beautifyJs(rendered), function (err, data) {
    if (err) { console.log(err) } else {
      console.log('written dest file', file)
    }
  })

  return template(data)
}

makeTrO(trad, tradObj)
