// functions to extract fields to translate from adapt's json's objects
var debug = require('debug')('trad:extract')

var _ = require('lodash')

var cs = require('./lib/loadCases')

var i = require('./lib/majordome')
var green = i.green
var red = i.red
var magenta = i.magenta
var buildColor = i.buildColor

var wrTr = require('./wrTradMod')
var makeTrO = wrTr.makeTrO
var replaceFile = wrTr.replaceFile

/**
 * search keys in config.json
 *
 * @param {object} obj adapt page object
 */
function transConf (obj, tArr) {
  debug(green('conf spotted !!!!'))
  var conf = {
    o: obj,
    t: tArr
  }
  cs.config(conf)
  return conf
}
/**
 * iterate all pages obj from contentObject.json
 *
 * @param {array} col array of adapt page objects
 */
function transPage (col, tArr) {
  const pageArr = []
  _.forEach(col, function (obj) {
    debug(green('page spotted !!!!'))
    var conf = {
      o: obj,
      t: tArr
    }
    pageArr.push(cs.page(conf).o)
  })
  return pageArr
}

/**
 * iterate all article obj from articles.json
 *
 * @param {array} col array of adapt page objects
 */
function transArt (col, tArr) {
  const artArr = []
  _.forEach(col, function (obj) {
    debug(green('article spotted !!!!'))
    var conf = {
      o: obj,
      t: tArr
    }
   artArr.push(cs.article(conf).o)
  })
  return artArr
}


/**
  * search keys in course.json
 *
 *
 * @param {object} obj adapt course object
 */
function transCourse (obj, tArr) {
  debug(green('course spotted !!!!'))
  var conf = {
    o: obj,
    t: tArr
  }
  cs.course(conf)
  return conf.o
}

/**
* iterate all components from the components.json array
* @param {array} col array of adapt page objects
*/
function transComps (col, tArr) {
  const compArr = []
  _.forEach(col, function (obj) {
   compArr.push(transComp(obj, tArr))
  })
  return compArr
}

/**
 * translate an component and is sub key, with case detection based
 * on obj '_component' key
 * @param  {object} obj    adapt object
 * @param  {Array} tArr  array to push translation pair to
 * @return {object}        [description]
 */
function transComp (obj, tArr) {
  var conf = {
    o: obj,
    t: tArr
  }

  debug(green('component spotted !!!!', conf.id))
  switch (obj._component) {
    case 'accordion':
      debug(magenta('accoridion spotted'))
      cs.accordion(conf)
      break
    case 'graphic':
      debug(magenta('graphic spotted'))
      cs.graphic(conf)
      break
    case 'hotgraphic':
      debug(magenta('hotgraphic spotted'))
      cs.hotgraphic(conf)
      break
    case 'hotgraphicCustom':
      debug(magenta('hotgraphicCustom spotted'))
      cs.hotgraphic(conf)
      break
    case 'flip':
      debug(magenta('flip spotted'))
      cs.flip(conf)
      break
    case 'matching':
      debug(magenta('matching spotted'))
      cs.matching(conf)
      break
    case 'mcq':
      debug(magenta('MCQ spotted'))
      cs.mcq(conf)
      break
    case 'media':
      debug(magenta('media spotted'))
      cs.media(conf)
      break
    case 'narrative':
      debug(magenta('narrative spotted'))
      cs.narrative(conf)
      break
    case 'slider':
      debug(magenta('slider spotted'))
      cs.slider(conf)
      break
    case 'text':
      debug(magenta('text spotted'))
      cs.text(conf)
      break
    case 'vinci_text':
      debug(magenta('vinci_text spotted'))
      cs.text(conf)
      break
    case 'textinput':
      debug(magenta('textinput spotted'))
      cs.textinput(conf)
      break
    case 'assessmentResults':
      debug(magenta('assessmentResults spotted'))
      cs.assessmentResults(conf)
      break
    default:
      if (obj._component !== 'undefined') {
        console.log(buildColor('unknown component'), red(obj._component), 'in', green(obj._id))
      }
      debug('regular type', green(obj._component))
  }
  return conf.o
}

module.exports = {
  transArt,
  transComps,
  transConf,
  transCourse,
  transPage,
  makeTrO,
  replaceFile
}
