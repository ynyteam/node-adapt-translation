var debug = require('debug')('extractObject')
var treatFile = require('./treatFile')
var fs = require('fs-extra')
var _ = require('lodash')
var path = require('path')
var fileArt = fs.readJsonSync('./src/course/en/articles.json')
var fileBlocks = fs.readJsonSync('./src/course/en/blocks.json')
var fileComponents = fs.readJsonSync('./src/course/en/components.json')
var fileCo = fs.readJsonSync('./src/course/en/contentObjects.json')
var fileCourse = fs.readJsonSync('./src/course/en/course.json')
var fileConfig = fs.readJsonSync('./src/course/config.json')
var cs = require('./lib/loadCases')


var hasProblem = 0
var problemArr = []
var h = require('./lib/helpers')


function treatObject (obj, transform) {
  var dataObj = JSON.parse(obj.serialised)
  var conf = {o: dataObj, t: []}
  var translated = transform(conf)
  obj.transformed = translated.o
  obj.tradArray = translated.t
  return obj
}

function treatAllObj (col) {
  // reset the id counter
  h.idCount = 0
  var allObj = []
  _.each(col, function (obj) {
    debug(obj.id, 'object is a', obj.type)
    switch (obj.type) {
      case 'article':
        debug('article spotted')
        allObj.push(treatObject(obj, cs.article))
        break
      case 'block':
        debug('block spotted')
        allObj.push(treatObject(obj, cs.article))
        break
      case 'component':
        debug('component spotted')
        switch (obj.cType) {
          case 'accordion':
            debug('accoridion spotted')
            allObj.push(treatObject(obj, cs.accordion))
            break
          case 'graphic':
            debug('graphic spotted')
            allObj.push(treatObject(obj, cs.graphic))
            break
          case 'hotgraphic':
            debug('hotgraphic spotted')
            allObj.push(treatObject(obj, cs.hotgraphic))
            break
          case 'matching':
            debug('matching spotted')
            allObj.push(treatObject(obj, cs.matching))
            break
          case 'mcq':
            debug('MCQ spotted')
            allObj.push(treatObject(obj, cs.mcq))
            break
          case 'media':
            debug('media spotted')
            allObj.push(treatObject(obj, cs.media))
            break
          case 'narrative':
            debug('narrative spotted')
            allObj.push(treatObject(obj, cs.narrative))
            break
          case 'text':
            debug('text spotted')
            allObj.push(treatObject(obj, cs.text))
            break
          case 'textinput':
            debug('textinput spotted')
            allObj.push(treatObject(obj, cs.textinput))
            break
          case 'slider':
            debug('slider spotted')
            allObj.push(treatObject(obj, cs.slider))
            break
          case 'assessmentResults':
            debug('slider spotted')
            allObj.push(treatObject(obj, cs.assessmentResults))
            break
          default:
            if (obj._component !== 'undefined') {
              ++hasProblem
              problemArr.push('component', obj._component, 'in', obj._id, 'is of an unkown type')
              // console.log('component', obj._component, 'in', obj._id, 'is of an unkown type')
            }
            debug('regular type', obj._component)
        }
        break
      case 'page':
        debug('page spotted')
        allObj.push(treatObject(obj, cs.page))
        break
      case 'course':
        debug('page spotted')
        allObj.push(treatObject(obj, cs.course))
        break
      case 'config':
        console.log('config spotted')
        allObj.push(treatObject(obj, cs.config))
        break
      default:
        if (obj.type !== 'undefined') {
          ++hasProblem
          problemArr.push('item type', obj.type, 'in', obj.id, 'is unknow please add case')
        }
    }
  })
  debug('all object from file had been treated')

  if (hasProblem > 0) {
    console.log('there was problems during the run:')
    _.each(problemArr, function (pb) {
      console.log(red(pb))
    })
  } else {
    console.log('no Problem during the extraction process for ', col[0].type, '!')
  }
  return allObj
}


function extractTrad (tradArray) {
  var allTrads = []

  _.each(tradArray, function (tradItem) {
    _.each(tradItem.tradArray, function (tradEntry) {
      allTrads.push(tradEntry)
    })
  })
  return allTrads
}

function makeAdaptJson (tradArray, isObject) {
  if (isObject === true) {
    return tradArray.transformed
  } else {
    debug('non obj')
    var adaptJson = []
    _.each(tradArray, function (tradItem) {
      adaptJson.push(tradItem.transformed)
    })
    return adaptJson
  }
}

function extractFromFile (data, fName) {
  var basePath = './out/' + fName

  fs.ensureDirSync(basePath)

  var treatedFile = treatFile(data)
  var testTransform = treatAllObj(treatedFile)
  var allTradLines = extractTrad(testTransform)
  var adaptJson = makeAdaptJson(testTransform, false)
  fs.writeJsonSync(path.join(__dirname, basePath, 'treatedFile.json'), treatedFile)
  fs.writeJsonSync(path.join(__dirname, basePath, fName + '.json'), adaptJson)
  fs.writeJsonSync(path.join(__dirname, basePath, 'testTransform.json'), testTransform)
  fs.writeJsonSync(path.join(__dirname, basePath, 'allTradLines' + fName + '.json'), allTradLines)
}

function init (treat) {
  h.clrDirs()
  h.chkDirs()
  if (treat) {
    extractFromFile(fileArt, 'article')
    extractFromFile(fileBlocks, 'blocks')
    extractFromFile(fileComponents, 'components')
    extractFromFile(fileCo, 'contentObjects')
    extractFromFile(fileCourse, 'course')
    extractFromFile(fileConfig, 'config')
  }
}

init(true)

module.exports = extractFromFile
