var fs = require('fs-extra')
var path = require('path')
var parse = require('csv-parse')
var _ = require('lodash')

var inFile = process.argv[2] || 'test.csv'

var input = fs.readFileSync(path.join(__dirname, '/', inFile), 'utf-8')
fs.ensureDirSync('./extracted/audio')
function makeEntry (data) {
  var regex = /Video/
  if (data[0].match(regex)) return
  var entry = {}
  entry.data = {}
  entry.data.tcIn = data[3]
  entry.data.tcOut = data[4]
  entry.file = data[0]
  if (data[6] !== '') entry.data.text = data[6]
  else entry.data.text = data[5]
  // console.log(entry)
  return entry
}

function makeTradArray (csvData) {
  var tradArr = []
  parse(csvData, {delimiter: ';'}, function (err, output) {
    if (err) return console.error(err)
    output.shift()

    _.forEach(output, function (data) {
      var entry = makeEntry(data)
      if (entry !== undefined) tradArr.push(makeEntry(data))
    })

    separateTexts(tradArr)

  // fs.writeJSONSync(path.join(__dirname, '/dist/audio.json'), tradArr)
  })
}

function separateTexts (obj) {
  var currFile
  var sepTexts = {}
  _.each(obj, function (v, k) {
    var line
    // var line = v.data.text.replace(/,/g, '&#44;').replace(/\r\n/g, ' ')
    // console.log(typeof v, k)
    if (currFile !== v.file) {
      line = makeWebVttHeader()
      line = 'WEBVTT\n\n' + treatData(v.data)
      // console.log('v', line)
      // console.log('v', v)
      sepTexts[v.file] = line
      currFile = v.file
    // console.log('da linea', line)
    // console.log(currFile)
    } else {
      sepTexts[v.file] = sepTexts[v.file] + treatData(v.data)
    }
  })
  _.each(sepTexts, function (v, k) {
    var title = k.slice(0, -3) + 'vtt'
    fs.writeFileSync(path.join(__dirname, '/extracted/audio/', title), v)
  })

  fs.writeJSONSync(path.join(__dirname, './test.json'), sepTexts)
}

makeTradArray(input)

function makeWebVttHeader () {
  return 'WEBVTT\n\n'
}

function treatData (data) {
  var webVttEntry
  webVttEntry = data.tcIn + ' --> ' + data.tcOut + '\n' + data.text + '\n\n'
  return webVttEntry
}
