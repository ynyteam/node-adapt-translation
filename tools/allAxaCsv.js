const fs = require('fs-extra')
const clean = require('./axaCsv')
const path = require('path')
const dirName = '../extract'

fs.readdir(path.join(__dirname, dirName), function (err, fileNames) {
  if (err) {
    return console.log(err)
  }
  // console.log(fileNames)
  fileNames.forEach(function (fileName) {
    if (fileName === '.DS_Store') return
    // console.log(fileName)
    fs.readFile(dirName + '/' + fileName, 'utf-8', function (err, content) {
      if (err) {
        console.log(err)
        return
      }
      clean(content, fileName)
    })
  })
  // fileNames.forEach(function (fileName) {
  // })
})
