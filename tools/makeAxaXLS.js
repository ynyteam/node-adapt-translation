/* make AXA XLS
this is a rudimentary tool to make Ofelia Repes happy
it take an XLS file provided as the Variable 'workbook' and out put a 'out.xlsx' file,
stripped of the HTML as some rendered formatted plain text with a fixed width of
*/

var XLSX = require('xlsx')
var fileName = process.argv[2] || 'mod5.xlsx'
var outName = process.argv[3] || 'out.xlsx'
var workbook = XLSX.readFile(fileName)
var htmlToText = require('html-to-text')
var sheetNameList = workbook.SheetNames

/* function to convert all string of non empty cell from html => plain text */
function toText (sheet) {
  for (var z in sheet) {
    /* all keys that do not begin with "!" correspond to cell addresses */
    if (z[0] === '!') continue
    sheet[z].v = htmlToText.fromString(sheet[z].v) // convert Html to
      .replace(/\[([^]]+)\]/g, '') // stip image tag from the conversion
  }
}

/* iterate through sheets */
sheetNameList.forEach(function (y, i) {
  var worksheet = workbook.Sheets[y]
  switch (y) {
    case 'components':
      toText(worksheet)
      console.log('components spotted')
      break
    case 'course':
      toText(worksheet)
      console.log('course spotted')
      break
    default:
      delete workbook.Sheets[y]
      sheetNameList.splice(i, 1)
      console.log('deleted', y)
  }
})
/* write down the sheet */
XLSX.writeFile(workbook, outName)
