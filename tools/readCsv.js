var fs = require('fs-extra')
var path = require('path')
var parse = require('csv-parse')
var _ = require('lodash')

var input = fs.readFileSync(path.join(__dirname, '/src/course-04-utf8.csv'), 'utf-8')
// console.log(input)

function makeEntry (data) {
  var entry = {}
  entry[data[0]] = data[2]
  // console.log(entry)
  return entry
}

function makeTradArray (csvData) {
  var tradArr = []
  parse(csvData, function (err, output) {
    if (err) console.error('carammmba', err)
    _.forEach(output, function (data) {
      tradArr.push(makeEntry(data))
    })
    fs.writeJSONSync(path.join(__dirname, '/extracted/tradEs-04.json'), tradArr)
    console.log('conversion to json done for file : tradEs.json')
  })
}

makeTradArray(input)
