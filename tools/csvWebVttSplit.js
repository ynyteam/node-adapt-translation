/**
 * this script take a the buffer of a csv file containig all audio translation as input and output an object containig splitted VALID csv strings
 */
var async = require('async')

function cleanCsv (data) {
  async.waterfall([
    async.apply(removeEmplyLines, data)
  ], function (err, result) {
    // result now equals 'done'
    console.log(result)
    return result
  })
}

function removeEmplyLines (data, callback) {
  callback(null, data.replace(/^\s*\n/gm, ''))
}

module.exports = cleanCsv
