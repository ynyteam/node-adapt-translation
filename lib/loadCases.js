/**
 * loads all cases in the cases dir in a single object to require
 * TODO switch to glob
 */
var debug = require('debug')('loadCase')

var fs = require('fs-extra')
var path = require('path')
var dir = path.join(__dirname, 'cases')
var cases = {}
var files = fs.readdirSync(dir)
files.map(function (file) {
  return path.join(dir, file)
}).filter(function (file) {

  if (/^.D/g.test(path.basename(file)) === false) {
  	                    return fs.statSync(file).isFile()
  } else {
  	                    return false
  }
}).forEach(function (file) {

  var fName = path.basename(file, '.js')
  var filePath = path.join(file)
  cases[fName] = require(filePath)
  return cases
})

module.exports = cases
