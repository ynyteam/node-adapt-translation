/* init functions (check dirs etc, etc )*/

var fs = require('fs-extra')
var conf = require('../config')

var majordome = {}

var chalk = require('chalk')
majordome.red = chalk.bold.red
majordome.green = chalk.bold.green
majordome.magenta = chalk.bold.magenta
majordome.yellow = chalk.bold.yellow
majordome.buildColor = chalk.bold.cyan
majordome.blue = chalk.bold.blue

function checkDirs () {
  // checks for directory that will be needed for the script
  conf.ensuredDirs.forEach(function (element, index, array) {
    fs.ensureDir(element, function (err) {
      if (err) {
        console.log(err) // => null
      }
    })
  })
}

function cleanup (cb) {
  conf.dirs.forEach(function (element, index, array) {
    if (element !== 'src' || element !== 'def') fs.removeSync(element)
  })
  if (cb) { cb() }
}

function balaie (dirs) {
  if (!dirs) dirs = conf.dirs
  dirs.forEach(function (element, index, array) {
    fs.emptyDirSync(element)
  })
}

function init () {
  cleanup(checkDirs)
}

majordome.cd = checkDirs
majordome.cl = cleanup
majordome.bal = balaie
majordome.init = init

module.exports = majordome
