/**
 * utilities functions
 */
var debug = require('debug')('trad:helpers')
var nb = require('node-beautify')
var fs = require('fs-extra')
var path = require('path')
var _ = require('lodash')
// var conf = require('../config')
var objectPath = require('object-path')

var i = require('./majordome')
var green = i.green
var red = i.red

// needed directories
var cleanDirs = ['extracted', 'out', 'dest', 'toTreat']
var allDirs = ['src', ...cleanDirs]

/**
* Helpers
*/

var helpers = {
  idCount: 0 // counter for makeUId
}

/**
 * write a json file with th given data, prettyfied, DEBUG
 * @param  {obj/arr}    data data to write
 * @param  {string}     file path to a file
 */

helpers.wrT = function (data, file) {
  fs.writeFileSync(file, nb.beautifyJs(JSON.stringify(data)))
  console.log(green(file, 'written!'))
}

helpers.getJsonObj = function (file) {
  return JSON.parse(fs.readFileSync(file, 'utf8'))
}

/**
* get a key using objectpath module
* @param  {object} obj     object to search
* @param  {string} a       key to search, can be nested
* @return {???}            return the key
*/
helpers.getKey = function (obj, a) {
  return objectPath.get(obj, a)
}

/**
* set a key using objectpath module
* @param  {object} obj     object to search
* @param  {string} a       key to seah, can be nested
* @param {???}     val     data to set the value to
*/
helpers.setKey = function (obj, a, val) {
  return objectPath.set(obj, a, val)
}

/**
* contatenate the name of the translated key and a unique number
* @param  {string}  key  name of the translated key
* @param  {int}     count  unique number
* @return {string}
*/

helpers.makeUId = function (key, iter) {
  iter = typeof iter !== 'undefined' ? iter : helpers.idCount
  var keysArr = key.split('.')
  var lastKey = keysArr[keysArr.length - 1]
  var uId = lastKey + iter
  ++helpers.idCount
  // console.log('helper val',helpers.idCount)
  return uId
}

/**
*  make lodash strings
*/
helpers._toTmpl = function (str) {
  return '<%= ' + str + ' %>'
}

/**
 * checks if a value is a lodash tempate string
 */
helpers.checkVal = function (val, key, obj) {
  key = typeof key !== 'undefined' ? key : 'noKey'
  obj = typeof obj !== 'undefined' ? obj : 'noObj'
  var regex = /<%([^%>]+)?%>/
  if (regex.test(val) === true) {
    debug('Warning translated key is a template', red(val))

    return true
  } else return false
}
 /*
  * check if needed dir exist, and create them if needed
  */
helpers.chkDirs = function (dirArr) {
  dirArr = dirArr || allDirs
  _.each(dirArr, function (dir) {
    fs.ensureDirSync(path.join(__dirname, '../', dir))
  })
  console.log('all directories needed are present/created')
}

helpers.clrDirs = function (dirArr) {
  dirArr = dirArr || cleanDirs
  _.each(dirArr, function (dir) {
    fs.removeSync(path.join(__dirname, '../', dir))
  })
  console.log('all directories are cleaned')
}

module.exports = helpers
