/**
 * make a cvs with the translation.json file
 */
var fs = require('fs-extra')
var stringify = require('csv-stringify')
var _ = require('lodash')
var path = require('path')
// var stripTags = require('striptags')
var htmlToText = require('html-to-text')

var i = require('./majordome')
var green = i.green

var columns = {
  id: 'référence',
  tradOrig: 'texte original',
  tradDest: 'texte traduit',
  stripped: 'texte sans balises',
  axaTrad: 'texte traduction client',
  axaComment: 'commentaire par client'
}

// make an arry of arrays for the csv file
function treatObjArr (arr) {
  var outArr = []
  arr.forEach(function (d, i) {
    var cArr = []
    _.forEach(d, function (value, key) {
      cArr.push(key, value)
    })

    // check if the string contain closin html markup ('</')
    // if (cArr[1].match(/<\//)) {
    //   cArr[3] = stripTags(cArr[1])
    // }
    cArr[3] = htmlToText.fromString(cArr[1], {uppercaseHeadings:false})
    .replace(/\n/, '\n ')
    outArr.push(cArr)
  })
  return outArr
}

// treat array of object and write a csv file
function toCsvFile (arr, file) {
  stringify(treatObjArr(arr), { header: true, columns: columns }, function (err, output) {
    if (err) console.log(err)
    var fileName = file.split('.')[0]
    fs.ensureDirSync(path.join(__dirname, '../extracted/csv/'))
    var tpath = path.join(__dirname, '../extracted/csv/', fileName + '.csv')
    fs.writeFile(tpath, output, function (err) {
      if (err) return console.log(err)
      console.log(green(tpath, 'written!'))
    })
  })
}

module.exports = toCsvFile

// toCsvFile2(testO,'contentObjects.txt')
