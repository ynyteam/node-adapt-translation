/**
 * narrative component js reader
 */
var tObj = require('./../transformers')

var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  '_graphic.alt',
  '_graphic.attribution'
]

module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  return conf
}
