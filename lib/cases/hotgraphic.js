/**
 * hotgraphic component js reader
 */
var tObj = require('./../transformers')
var _ = require('lodash')

var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  'mobileBody',
  'mobileInstruction',
  '_graphic.alt'
]

var itemFields = [
  'title',
  'body',
  'pinAlt',
  'strapline',
  '_graphic.alt'
]

module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  _.forEach(conf.o._items, function (item) {
    transFields(item, itemFields, conf.t)
  })
  return conf
}
