var tObj = require('./../transformers')
var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  '_buttons._submit.buttonText',
  '_buttons._submit.ariaLabel',
  '_buttons._reset.buttonText',
  '_buttons._reset.ariaLabel',
  '_buttons._showCorrectAnswer.buttonText',
  '_buttons._showCorrectAnswer.ariaLabel',
  '_buttons._hideCorrectAnswer.buttonText',
  '_buttons._hideCorrectAnswer.ariaLabel',
  '_buttons._showFeedback.buttonText',
  '_buttons._showFeedback.ariaLabel',
  '_buttons.remainingAttemptsText',
  '_buttons.remainingAttemptsText',
  '_feedback.correct',
  '_feedback._incorrect.notFinal',
  '_feedback._incorrect.final',
  '_feedback._partlyCorrect.notFinal',
  '_feedback._partlyCorrect.final',
  'labelEnd',
  'labelStart'
]

module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  return conf
}

module.exports.fields = baseFields
