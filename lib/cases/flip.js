var tObj = require('./../transformers')
var eachCol = tObj.eachCol
var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'front',
  'back',
  'instruction',
  'body'
]


module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  return conf
}
