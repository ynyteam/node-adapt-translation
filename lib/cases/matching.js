/**
 * matching question component js reader
 */
var tObj = require('../transformers')
var _ = require('lodash')
var transKey = tObj.tKey
var transFields = tObj.transFields
var eachCol = tObj.eachCol
var assert = require('assert')
var i = require('../majordome')
var red = i.red

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  '_feedback.correct',
  '_feedback._partlyCorrect.notFinal',
  '_feedback._partlyCorrect.final',
  '_feedback._incorrect.notFinal',
  '_feedback._incorrect.final',
  'placeholder'
]
/**
 * Matching component can override buttons
 */
var coreButtonsFields = [
  '_buttons._submit.buttonText',
  '_buttons._submit.ariaLabel',
  '_buttons._reset.buttonText',
  '_buttons._reset.ariaLabel',
  '_buttons._showCorrectAnswer.buttonText',
  '_buttons._showCorrectAnswer.ariaLabel',
  '_buttons._hideCorrectAnswer.buttonText',
  '_buttons._hideCorrectAnswer.ariaLabel',
  '_buttons._showFeedback.buttonText',
  '_buttons._showFeedback.ariaLabel',
  '_buttons.remainingAttemptsText',
  '_buttons.remainingAttemptText',
  '_buttons.disabledAriaLabel'
]

baseFields = baseFields.concat(coreButtonsFields)
module.exports = function (conf) {
  assert.ok(typeof conf.o === 'object',
    red('non standard conf fill, missing data'))
  transFields(conf.o, baseFields, conf.t)
  _.forEach(conf.o._items, function (item) {
    transKey(item, 'text', conf.t)
    eachCol(item._options, ['text'], conf.t)
  })
  return conf
}
