var tObj = require('./../transformers')
var transFields = tObj.transFields

var baseFields = [
  '_defaultLanguage',
  '_spoor._reporting._comment',
  '_spoor._reporting._onTrackingCriteriaMet',
  '_spoor._reporting._onAssessmentFailure'
]

module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  return conf
}

module.exports.fields = baseFields
