var tObj = require('./../transformers')
var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'pageBody',
  'linkText',
  'instruction',
  'durationLabel',
  'duration',
  '_graphic.alt'
]

/**
 * conf:{
 *  o : obj,
 *  f:fieldsArr,
 *  t:tArr
 * }
 */
module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  return conf
}
