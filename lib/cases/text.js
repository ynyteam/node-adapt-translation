var tObj = require('./../transformers')
var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction'
]

module.exports = function (conf) {
  transFields(conf.o, baseFields, conf.t)
  return conf
}

module.exports.fields = baseFields
