const fs = require('fs-extra')
const path = require('path')
const parse = require('csv-parse')
const stringify = require('csv-stringify')
const _ = require('lodash')
const striptags = require('striptags')
const Entities = require('html-entities').AllHtmlEntities
// const detectNewline = require('detect-newline')

const entities = new Entities()

function makeTradArray (csvData, fileName) {
  parse(csvData, function (err, output) {
    if (err) console.error('carammmba', err)
    // console.log(output)
    // if()
    // console.log(output)
    stringify(output, function (err, output) {
      if (err) throw err
      fs.writeFileSync(path.join(__dirname, '/extracted/', fileName), output, 'utf-8')
      console.log('conversion to cleaned csv done for file :', fileName)
    })
  })
}


module.exports = makeTradArray
