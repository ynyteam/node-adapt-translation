# PROCÉDURE D'UTILISATION DE L'OUTIL DE TRADUCTION :

////////////////////////////////////////////////////////////////////////////////////

# 1. PRÉ-REQUIS

1.1
L'outil néccessite une version de node >= à 5.9 Vérifier le avec la cmd : $ node -v
Installer les dépendences de l'outil de traduction avec la cmd : $ npm i

1.2
Créer un dossier "src" à la racine du projet s'il n'existe pas.

////////////////////////////////////////////////////////////////////////////////////

# 2. EXTRACTION DES DONNÉES

2.1
Copier le dossier "course" que vous souhaitez traduire dans le répertoire "src"

2.2
Lancer la cmd : $ node extract

2.3
Les composants qui ne sont pas référencés par l'outil sont indiqués en rouge dans le terminal. Il est nécessaire de les ajouter au systeme de traduction (#3)

////////////////////////////////////////////////////////////////////////////////////

# 3. AJOUT DE NOUVEAUX COMPOSANTS À TRADUIRE

3.1
Créer un fichier MyComponent.js dans le dossier ./lib/cases sur la base de ceux existants. ("MyComponent" étant le nom du nouveau composant à ajouter)

Notes :
- la variable "baseFields" liste les attributs à traduire dans le composant.
- la variable "itemFields" liste les attributs à traduire dans le tableau d'objets "_items" que peux contenir votre composant.

3.2
Éditer le fichier extract.js en ajoutant à la fonction "transComp" une nouvelle condition :

case 'MyComponent':
      debug(magenta('MyComponent spotted'))
      cs.MyComponent(conf)
      break

////////////////////////////////////////////////////////////////////////////////////

# 4. RÉCUPÉRATION DES DONNÉES EXTRAITES

4.1
Suite à la cmd "$ node extract" , un dossier "extracted" a été créé avec la sous arborescence suivante :

./extracted
	/course
	/csv
	/trad

"course" contient des versions modifiées des fichiers json où les champs à traduire ont été remplacé par des clés uniques.

"csv" contient un fichier "course.csv" reprenant les informations extraites sous forme de tableau :

"trad" contient un fichier "trad.json" reprenant les informations extraites ss le modèle :

[
	{
		"id_key1":"data extracted"
	},
	{
		"id_key2":"data extracted"
	},
	{...}
]

////////////////////////////////////////////////////////////////////////////////////

# 5. TRADUCTION DES DONNÉES EXTRAITES

5.1 Importer ./extracted/csv/course.csv dans une feuille de calcul de google drive (lors de l'import choisir : séparation par virgule)

5.2 Protéger de la modification de données les colonnes "référence" et "texte original"

5.3 Saisir la traduction des cellules de la colonne "texte original" dans les cellules correspondantes de la colonne "texte traduit"
(IMPORTANT : les balises html doivent être conservées)

5.4 Exporter / Télécharger au format Excel xlsx votre feuille de calcul (avec la colonne "texte traduit" remplie correctement bien sur)

5.5 Renommer le fichier exporté  NomFeuilleDeCalcul.xlsx en course.xlsx et copier le dans le dossier ./extracted/trad/course.xlsx

5.6 Lancer la commande : $ node ./tools/getFromGoogleSpreadsheetXlsx.js 

5.7 Le fichier ./extracted/trad/trad.json a été modifié avec les champs traduits.


////////////////////////////////////////////////////////////////////////////////////

# 6. ÉCRITURE DES DONNÉES TRADUITES

6.1 Le fichier utilisé par le système pour remplacer les clés uniques de référence aux champs traduits est le fichier ./extracted/trad/trad.json

6.2 Lancer la cmd : $ node wrTrad.js

6.3 Un dossier "dest" est créé. Il contient le dossier "course" avec les champs traduits

6.4 Recompiler votre module e-learning avec ce dossier "course"





